const {user} = require('../data');
module.exports = (app) => {
    //get api endpoint
    app.get('/', (req, res=>{
        return res.status(200).send()
    }))

    //get api endpoint
    app.get('/users', (req, res)=>{
        // return res.status(200).json(user);
        return res.send(users);
    })

    //get api endpoint
    app.post('/users', (req, res)=>{
        // return res.status(200).json(user);
        // return res.status(200).send({
        //     'Message': 'User created!'
        // });
        if(!req.body.hasOwnProperty('fullName')){
            return res.status(400).send({
                'Error': 'Bad Request - missing required parameter fullName'
            })
        }

        return res.status(200).send({
            'Message' : 'User created!'
        })
    })
}